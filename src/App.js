import logo from './logo.svg';
import './App.css';
import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
  useLocation,
  useHistory
} from "react-router-dom";

export default function App() {
  return (
    <Router>
      <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/about">About</Link>
          </li>
          <li>
            <Link to="/books">Books</Link>
          </li>
        </ul>

        <Switch>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/books">
            <Books />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

function Home() {
  return <h2>Home</h2>;
}

function About() {
  return <h2>About</h2>;
}

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

function Books() {
  let match = useRouteMatch();
  let query = useQuery();
  let defaultCategory = query.get("category");
  let history= useHistory()

  if (defaultCategory == null) {
    defaultCategory = 1
    handle("/books?category=1", history)
  }

  let categories = [
    {name: "Category1", id: 1 }, 
    {name: "Category2", id: 2 }, 
    {name: "Category3", id: 3 }
  ];
	let lis = categories.map(c => {
			return <Link to={`${match.url}?category=${c.id}`} key={`${c.id}`}>{c.name}</Link>
  })

  return (
    <div>
      <h2>Books</h2>
      <ul>
				{ lis }
      </ul>
			<Book category = {defaultCategory} />
    </div>
  );
}

async function handle(url, history) {
  await history.push(url)
}

function Book({category}) {
  return <h3>Requested book category: {category}</h3>;
}

